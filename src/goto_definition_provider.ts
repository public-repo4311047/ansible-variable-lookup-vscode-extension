import * as vscode from 'vscode';
import { project } from './extension';

export class GotoDefinitionProvider implements vscode.DefinitionProvider {
	public async provideDefinition(
		document: vscode.TextDocument,
		position: vscode.Position,
		token: vscode.CancellationToken
	): Promise<vscode.Definition | vscode.DefinitionLink[]> {
		const selectedWord = document.getText(document.getWordRangeAtPosition(position));
		const variableName = selectedWord.replace(/:$/, "");
		const results = project.find(variableName);

		const locations: vscode.Location[] = results.map(result => {
			return new vscode.Location(
				vscode.Uri.file(result.filePath),
				new vscode.Range(result.lineNumber - 1, 0, result.lineNumber, 0)
			);
		});

		return locations;
	}
}
