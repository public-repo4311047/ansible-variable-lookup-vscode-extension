import * as vscode from 'vscode';
import { parseAnsibleBuiltin, parseAnsibleBuiltinFilters, parseYaml } from './parser';
import { VariableTree, VariableValue } from './variable_tree';
import { isAnsibleVariable } from './ansible';
import { sortPrecedence } from './precedence';

/**
 * Represents a workspace Ansible project 
 * used to lookup variables
 */
export class WorkspaceProject{
    workspaceVariables: VariableTree = new VariableTree();

    /**
     *  Given a (normalized) variable, find all declarations of the variable
     */
    find(variable: string): VariableValue[] {
        
        const variableParts = variable.split('.');
        const mainVariable = variableParts[0];

        if (Object.keys(this.workspaceVariables).length === 0) {
            return [];
        }

        // Determine if we have hover an ansible variable or not
        if (!isAnsibleVariable(mainVariable)) {
            return [];
        }

        return sortPrecedence(this.workspaceVariables.get(variable));
    }

    /**
     * One time parses all files in the current workspace to find variable declarations and special variables.
     * Uses hardcoded glob patterns to limit the number of files to parse based on Ansible folder and extension naming rules.
     */
     parseWorkspace() {
        // Clear old values (if present)
        this.workspaceVariables = new VariableTree();

        // Add the fixed builtin Ansible variables
        parseAnsibleBuiltin(this.workspaceVariables);
        parseAnsibleBuiltinFilters(this.workspaceVariables);

        // Try to reduce the amount of files found,
        // we want .yml, .yml and no extension but the latter is hard to do with glob
        const fileScanInclude: string = "**/*";
        const fileScanExclude: string = "{**/collections/**,**/venv/**,**/prererequisites/**,**/files/**,**/templates/**,**/*.md,**/*.png,**/*.zip,**/*.gz,**/*.tar}";
        
        vscode.workspace.findFiles(fileScanInclude, fileScanExclude).then((files) => {

            files.forEach((file) => {
                this.parseFile(file.fsPath);
            });
        });        
    }

    /**
     * Parse only a single file, update occurences of variable if they already exists for the given file.
     * Can be used if a file was changed.
     * To parse a whole project use parseWorkspace().
     * 
     * @param path File path as a string
     */
    parseFile(path: string){
        parseYaml(this.workspaceVariables, path);        
    }

    /**
     * Remove variables from the workspace that are no longer in the workspace
     */
    deleteFile(path: string){        
        this.workspaceVariables.removeForFile(path);
    }
}