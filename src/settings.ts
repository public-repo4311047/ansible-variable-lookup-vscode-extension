import * as vscode from 'vscode';

const defaultEnableOnLanguages = ['yaml', 'ansible', 'shellscript', 'jinja', 'ansible-jinja'];

/**
 * Load settings, settings should be declared in package.json under the contributes/configuration section.
 */
export class Settings {
    enableHover: boolean = true;
    enableOnLanguages: string[] = defaultEnableOnLanguages;
    truncateHoverLinkLength: number = 40;
    truncateHoverValueLength: number = 80;

    constructor() {
        this.load();
        this.listen();
    }

    load(){       
        const config = vscode.workspace.getConfiguration("ansible-variable-lookup");
       
        this.enableHover = config.get('enableHover', true);
        this.truncateHoverLinkLength = config.get('truncateHoverLinkLength',  40);
        this.truncateHoverValueLength = config.get('truncateHoverValueLength',  80);
        this.enableOnLanguages = config.get('enableOnLanguages', `${defaultEnableOnLanguages}`).split(',').map(s => s.trim());
    }

    listen(){
        vscode.workspace.onDidChangeConfiguration(event => {
            this.load();
        });
    }
}