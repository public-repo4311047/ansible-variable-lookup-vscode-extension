import * as path from 'path';

// Words not to lookup as variables
const keywords = new Set<string>(["bool", "ansible", "builtin", "item", "false", "true", "str", "when", "include", "loop_control", "with_items", "vars", "hosts", "False", "await", "else", "import", "pass", "None", "break", "except", "in", "raise", "True", "class", "finally", "is", "return", "and", "continue", "for", "lambda", "try", "as", "def", "from", "nonlocal", "while", "assert", "del", "global", "not", "with", "async", "elif", "if", "or", "yield", "name", "tasks"]);

// Check if valid characters for a variable
// must start with a character or _, followed by any valid char
const validNameRegex = /^[a-zA-Z_]{1}[a-zA-Z0-9_\.]+$/;

/*
 * Try to determine if a word might be an ansible variable
 * Used for determining if we need to enable the hover
 */
export function isAnsibleVariable(variable: string): boolean {
	const normalized = normalizeVariable(variable);
	return normalized !== null && !keywords.has(normalized) && !normalized.startsWith("ansible.builtin.") && validNameRegex.test(normalized);
}

// Try to determine if a file is an Ansible file
export function isAnsibleFile(filePath: string): boolean {
	filePath = filePath.toLocaleLowerCase();
	
	const fileName = path.basename(filePath);
    const extension = path.extname(filePath);
	const hiddenFile = fileName.startsWith(".");

	if(hiddenFile){
		return false;
	}

	if(isInventoryFile(filePath)){
		return true;
	}

	if(extension === ".yml" || extension === ".yaml"){
		return true;
	}

	// In group_vars or host_vars the following is valid Ansible, no extension, yml, yaml or json (or even ini but no support yet in this extension)
	if( (extension === "" || extension === ".json") && (filePath.includes("host_vars") || filePath.includes("group_vars")) ) {
		return true;
	}

	// All other is not an Ansible file
	return false;
}

// Try to determine if a file is an inventory file
export function isInventoryFile(filePath: string): boolean {
	const fileName = path.basename(filePath).split(".")[0];

	return ["hosts", "inventory"].includes(fileName);
}

// Normalize variable to dot notation and trim qoutes
export function normalizeVariable(input: string): string {
	// Skip filters after variable
	const skipFilter = input.split('|')[0];
	// Rewrite var['sub'] or var["sub"] to var.sub
    const dotNotation = skipFilter.replace(/\[['"]([^'"]+)['"]]/g, '.$1');
	// "my_var" or 'my_var' to my_var
	// trim ending ':'
	const trimmedNotation = dotNotation.replace(/^["'\s+]/, '').replace(/["':\s+]$/, '');

	return trimmedNotation;
}