import * as vscode from 'vscode';

/**
 * Simple structure for a search hit
 */ 
export class VariableValue {
    /**
     * Absolute file path where the variable was found
     */
    filePath: string = "";
    /**
     * Relative file path to the workspace root folder
     */
	relativePath: string = "";
    /**
     * Line number were declaration starts
     */
	lineNumber: number = 0;
    /**
     * Optional parent object name (eg. vars,block,set_fact)
     */ 
    parent: string = "";
    /**
     * The Ansible precedence following the Ansible rules.
     * Runtime the value with the highest precedence will be used by Ansible.
     * https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_variables.html#variable-precedence-where-should-i-put-a-variable
     */
    precedence: number = -1;

	private parsedVariable: any = "";

    constructor(filePath: string, lineNumber: number, value: any) {
        this.filePath = filePath;
        this.relativePath = vscode.workspace.asRelativePath(filePath);
        this.lineNumber = lineNumber;
        this.parsedVariable = value;
    }

    /**
     * Sometimes a variable is a dict like interface.name, in that case lookup 'name' in the dict
     */
	public value(path: string = ""): any {

        const variableParts = path.split('.');
		
		// In case result is a dict, browse into the dict
		if (variableParts.length > 1){
			// We already found the main variable, take the subpart
			let sub = variableParts.slice(1).join('.');
			return this.getPropertyByPath(this.parsedVariable, sub);
		}

        return this.parsedVariable;
    }

    /*
     * ChatGPT generated
     */
    public valueToString(path: string = ""): string {
        const value = this.value(path);
        if (typeof value === 'string') {
            return value;
        } else if (Array.isArray(value)) {
            if (value.length === 0) {
                return '[]';
            } else if (typeof value[0] === 'string') {
                return value.join(', ');
            } else if (typeof value[0] === 'object') {
                return value.map((item: any) => JSON.stringify(item)).join(', ');
            } else {
                return value.toString();
            }
        } else if (typeof value === 'object') {
            return JSON.stringify(value);
        } else {
            return value.toString();
        }
    }

    /*
     * ChatGPT generated
     */
    private getPropertyByPath(obj: any, path: string): any {
        const keys = path.split('.');
        let current = obj;
        for (const key of keys) {
            if (!current || typeof current !== 'object') {
                return undefined; // Handle cases where the path is invalid or leads to non-object values
            }
            current = current[key];
        }
        return current;
    }
    
}

interface variableLookupDict {
    [key: string]: VariableValue[];
}

// A dict with the variable name as key, and a VariableValue list as value 
export class VariableTree {
    variables: variableLookupDict = {};

    add(variableName: string, variableValue: any) {
        // Discard numeric results in case it tries to add arrays without parsing
        if (!isNaN(Number(variableName))) {
            return;
        }

        if (!this.variables[variableName]) {
            this.variables[variableName] = [];
        }
        
        this.variables[variableName].push(variableValue);
    }

    // Remove all entries coming from a given file,
    // this is usefull for refreshing vars after a file was changed and saved
    removeForFile(filePath: string){

        const relativePath = vscode.workspace.asRelativePath(filePath);
        
        // Keep in all entry where the file path is not the given file path
        for (let [key, list] of Object.entries(this.variables)) {
            this.variables[key] = list.filter(obj => obj['relativePath'] !== relativePath);
        }
    }

    /*
     * Return a list of found variables, empty list if not found
     */
    get(variableName: string): VariableValue[]{
        // In case of dicts, like interface.name, take first part as lookup 'interface'
        const variableParts = variableName.split('.');
	    const mainVariable = variableParts[0];

        if (this.variables[mainVariable]) {
            return this.variables[mainVariable];
        } 

        return [];
    }
}
