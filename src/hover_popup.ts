import * as vscode from 'vscode';
import { VariableValue } from './variable_tree';
import { WorkspaceProject } from './workspace_project';
import { Settings } from './settings';
import { normalizeVariable } from './ansible';

export class HoverPopup {
    project: WorkspaceProject;
    settings: Settings;

    constructor(project: WorkspaceProject, settings: Settings) {
        this.project = project;
        this.settings = settings;
    }

    /**
     * Format a popup to show on hovering a given location.
     * 
     * @param document 
     * @param position 
     * @returns 
     */
    createPopup(document: vscode.TextDocument, position: vscode.Position){
        // Get the hovered word
        const hoveredWord = normalizeVariable(document.getText(document.getWordRangeAtPosition(position)));
        const results = this.project.find(hoveredWord);

        // Nothing found
        if (results.length === 0){
            return undefined;
        }

        // Format a markdown table
        const message = new vscode.MarkdownString();
        message.appendMarkdown("|   |   |   |\n");
        message.appendMarkdown("| - | - | - |\n");

            // Format message
        results.forEach(result => {
            let locationText = "";
            if(this.isHoveringItself(document, position, result)){
                locationText = "here";				
            }
            else {
                const trunctatedLink = this.truncate(result['relativePath'], this.settings.truncateHoverLinkLength, '...');
                // In case of spaces in the directories, replace the with %20, such that the link works
                let filePath = result.filePath.replaceAll(' ', '%20');
                locationText = `[${trunctatedLink}](file:${filePath}#${result.lineNumber})`;
            }

            let precedenceText = `[*](estimated_precendence_${result.precedence})`;
            if(result.precedence > 30) {
                precedenceText = `[*](estimated_precendence_unkown)`;
            }		
            
            // Give the hoveredWord, such that in case of dict, we can browse into the variable
            let value = result.valueToString(hoveredWord);
            
            // Skip if we hover the declaration itself if there are no other declarations
            if(results.length === 1 && this.isHoveringItself(document, position, results[0])) {
                message.appendMarkdown(`| ${precedenceText} | Variable only | in ${locationText} |\n`);
                return new vscode.Hover(message);
            }
            
            // For builtin variables, do no truncate, we also have only one result, so room enough
            if(result.filePath === 'special_variables'){
                const formattedValue = result.value().replaceAll('`', '\\`').replaceAll('|','\\|');
                message.appendMarkdown(`| | \`${formattedValue}\` | in [ansible special](https://docs.ansible.com/ansible/latest/reference_appendices/special_variables.html) | |\n`);
            }
            else
            if(result.filePath === 'filter'){
                const formattedValue = result.value().replaceAll('`', '\\`').replaceAll('|','\\|');
                message.appendMarkdown(`| | \`${formattedValue}\` | in [ansible filter](https://docs.ansible.com/ansible/latest/collections/index_filter.html) | |\n`);
            }
            else
            if (result.parent === "register"){
                message.appendMarkdown(`| ${precedenceText} | Registered variable | in ${locationText} |\n`);
            }
            else 
            if (value.startsWith("$ANSIBLE_VAULT;")){
                const tail = value.trimEnd().substring(value.length - 3);
                message.appendMarkdown(`| ${precedenceText} | **vault encrypted** [...${tail}] | in ${locationText} |\n`);
            }
            else{				
                const escaped = value.replaceAll('`', '\\`').replaceAll('|','\\|');
                const formatted = this.truncate(escaped, this.settings.truncateHoverValueLength, '` ... `');
                message.appendMarkdown(`| ${precedenceText} | \`${formatted}\` | in ${locationText} |\n`);
            }
        }); 

        return new vscode.Hover(message);
    }


    /*
     * Are we hovering the variable definition itself?
     */
    private isHoveringItself(document: vscode.TextDocument, position: vscode.Position, variable: VariableValue): boolean {
        const hoveredFile = vscode.workspace.asRelativePath(document.fileName);	
        return variable.relativePath === hoveredFile && variable.lineNumber === position.line + 1;
    }

    /*
     * Truncate a string to max length by removing the middle part and replace it with a given filler string
     */
    private truncate(str: string, maxLength: number, filler: string = '...'): string {
        if (str.length <= maxLength) {
            return str; // no need to truncate
        }

        const truncatedLength = maxLength - filler.length;
        const beginning = str.slice(0, Math.ceil(truncatedLength / 2));
        const end = str.slice(str.length - Math.floor(truncatedLength / 2));
        // Return the concatenated string with the filler in the middle
        return `${beginning}${filler}${end}`;
    }
}