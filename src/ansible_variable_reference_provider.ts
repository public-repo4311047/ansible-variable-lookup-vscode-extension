import * as vscode from 'vscode';
import * as fs from 'fs';
import { isAnsibleVariable } from './ansible';

export class AnsibleVariableReferenceProvider implements vscode.ReferenceProvider {
	private static fileScanInclude: string = "{**/templates/**/*,**/*.yml,**/*.YML, **/*.yaml}";
	private static fileScanExclude: string = "{**/collections/**,**/venv/**,**/files/**,**/*.md,**/*.png,**/*.zip,**/*.gz,**/*.tar,**/*.tgz}";

	public async provideReferences(document: vscode.TextDocument, position: vscode.Position, options: { includeDeclaration: boolean; }, token: vscode.CancellationToken): Promise<vscode.Location[]> {
		const wordRange = document.getWordRangeAtPosition(position);
		const selectedWord = document.getText(wordRange);
		const variableName = selectedWord.replace(/:$/, "");

		if (!isAnsibleVariable(variableName)) {
			return [];
		}

		const allReferences: vscode.Location[] = [];
		
		const files = await vscode.workspace.findFiles(AnsibleVariableReferenceProvider.fileScanInclude, AnsibleVariableReferenceProvider.fileScanExclude);

		// Asyncronously search for references in all selected files
		// But wait for all to finish before continuing
		await Promise.all(
			files.map((file) => 				
				this.getReferences(file, variableName, allReferences)
			)
		);
		
		return allReferences;
	}

	public async getReferences(file: vscode.Uri, variable: String, references: vscode.Location[]): Promise<void> {
		// console.log(`Searching for references in ${file.fsPath}`);
		const doc = await fs.promises.readFile(file.fsPath, 'utf8');
		const regex = new RegExp(`\\b${variable}\\b(:?)`);

		const lines = doc.split('\n');
		
		for(let lineIndex = 0; lineIndex < lines.length; lineIndex++) {
			const lineText = lines[lineIndex];

			const match = lineText.match(regex);
			
			if (match && match.index !== undefined) {
				// Determine if the match is a declaration or a reference
				const isDeclaration = match[1] === ":";
			
				// Skip declaration of variable, but not if it is something like this:
				//
				// mydict:
                //   varname: "{{ varname }}"
				//
				// So if it occures twice, assume the second is a reference
				if (!isDeclaration || (isDeclaration && countWordInLine(lineText, variable) >= 2)) {
					
					// Offset index by 1 to account for the space or quote character
					const reference = new vscode.Location(file, new vscode.Position(lineIndex, match.index + 1));
					references.push(reference);

				}
			}			
		}

		// Count the number of times a word appears in a line
		function countWordInLine(line: String, word: String): number {
			const regex = new RegExp(`\\b${word}\\b`, 'g'); // 'g' flag for global search

			// Assert that the regex is working
			// const test_two_hits = "varname: {{ varname }}".match(regex);
			// assert(test_two_hits !== null && test_two_hits.length === 2);

			// const test_one_hit = "varname: {{ varnames }}".match(regex);
			// assert(test_one_hit !== null && test_one_hit.length === 1);

			const matches = line.match(regex);
			return matches ? matches.length : 0;
		}
		  
		// console.log(`Found ${references.length} references in ${file.fsPath}`);
	}
}
