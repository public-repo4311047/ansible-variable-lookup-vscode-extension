// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { WorkspaceProject } from './workspace_project';
import { HoverPopup } from './hover_popup';
import { GotoDefinitionProvider } from './goto_definition_provider';
import { AnsibleVariableReferenceProvider } from './ansible_variable_reference_provider';
import { Settings } from './settings';

const settings = new Settings();
export const project = new WorkspaceProject();
const hover = new HoverPopup(project, settings);

// This method is called when your extension is activated,
// activation conditions are set in the 'package.json' file
export function activate(context: vscode.ExtensionContext) {
        
	// Initially parse all variables
	project.parseWorkspace();

	// On a save action of a yaml file, reload the variables of that document
	let yamlFileSaved = vscode.workspace.onDidSaveTextDocument((document) => {
		project.parseFile(document.uri.fsPath);
	});

	// On a delete action of a yaml file, remove the variables of that document
	let yamlFilesDeleted = vscode.workspace.onDidDeleteFiles((document) => {
		for (const file of document.files) {
			project.deleteFile(file.fsPath);
		}
	});
	
	// Register a hover for yaml files
	let hoverProvider = vscode.languages.registerHoverProvider(settings.enableOnLanguages, {	
		provideHover(document, position, token) {
			if( settings.enableHover === false ){
				return undefined;
			}
			
			return hover.createPopup(document, position);			
		}
	});

	context.subscriptions.push(yamlFileSaved);
	context.subscriptions.push(yamlFilesDeleted);
	context.subscriptions.push(hoverProvider);

	const gotoProvider = new GotoDefinitionProvider();
	const referenceProvider = new AnsibleVariableReferenceProvider();

	settings.enableOnLanguages.forEach(language => {
		context.subscriptions.push(vscode.languages.registerDefinitionProvider(language, gotoProvider));
		context.subscriptions.push(vscode.languages.registerReferenceProvider(language, referenceProvider));
	});    

	if (settings.enableHover === true ) {
		vscode.window.showInformationMessage(`Activated Ansible Variable Lookup extension. Popup feature is enabled in settings.`);
	}else{
		vscode.window.showInformationMessage(`Activated Ansible Variable Lookup extension. Popup feature is currently disable in the settings. Use the contect menu to show results.`);
	}
}

// This method is called when your extension is deactivated
export function deactivate() {}
